package com.chlwodh97.jwttoken.configure;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class JwtTokenProvider {
    private final UserDetailsService userDetailsService;
    @Value("${spring.jwt.secret}")
    private String secretKey;

//    @PostConstruct // Post 뜻-> 먼저, Construct 뜻-> 구조
//    protected void init() {
//        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
//        // Base64 : 암호화 기법 단방향 방식 (옛날)
//    }


    private SecretKey key = null;

    @PostConstruct
    protected void init() {
        key = Keys.hmacShaKeyFor(secretKey.getBytes(StandardCharsets.UTF_8));
    }

    public String createToken(String username, String role, String type) {
        Claims claims = Jwts.claims().setSubject(username);
        // Claims -> 토큰 자체가 정보를 가지는 방식, "나야~" 하는 느낌
        claims.put("role",role); // role 뜻-> 역할
        Date now = new Date(); // 외국 접속 생각

        long tokenValidMillisecond = 1000L * 60 * 60 * 10; // 10시간 -> 웹 경우 아침에 출근 로그인 ~ 퇴근 까지 대충 10시간 이다.
        if (type.equals("APP")) tokenValidMillisecond = 1000L * 60 * 60 * 24 * 365; // 1년 -> 앱에서 매일 로그인 시키면 안된다.

        return Jwts.builder() // 빌드는 쌓는 역할
                .setClaims(claims)
                .setIssuedAt(now) // 언제 발급 되었다
                .setExpiration(new Date(now.getTime() + tokenValidMillisecond)) // 만료일 ~부터 ~까지 유효한 토큰
                .signWith(key, SignatureAlgorithm.HS256) // 위조 방지 사인
                .compact();
        // 만들고 리턴 -> 스트링 토큰
    }

    // 토큰을 분석 인증 정보를 가져옴 (Authentication -> 일회용 출입증)
    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    // 토큰을 파싱 -> username 을 가져옴
    // 토큰 생성시 username 은 subject 에 넣은 것 꼭 확인
    public String getUsername(String token) {
        return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody().getSubject();
    }

    // resolve 뜻-> 결정, 해결
    // 프론트 서버에 api 호출 전화 걸고 기다림
    // -> 실패 , 성공 모두 프로미스 -> resolve 는 프로미스 중에서 성공한 것

    // resolveToken 성공 토큰을 가져옴 -> 요청이 성공시 헤더에서 토큰값을 가져올 수 있음
    public String resolveToken(HttpServletRequest request) {
        //  base64로 인코딩 한 “사용자ID:비밀번호” 문자열 Basic 과 함께 인증 헤더에 입력.
        return request.getHeader(HttpHeaders.AUTHORIZATION);
        // AUTHORIZATION 인가 -> 인증을 허가 -> 권한이 있는지 확인 과정
    }

    // 유효 시간 검사 -> 지나면 false 준다
    public boolean validateToken(String jwtToken) {
        try {
            Jws<Claims> claims = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(jwtToken);
            return !claims.getBody().getExpiration().before(new Date());
        } catch (Exception e){
            return false;
        }
    }
}
