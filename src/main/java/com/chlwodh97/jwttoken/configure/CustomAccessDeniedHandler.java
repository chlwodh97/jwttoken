package com.chlwodh97.jwttoken.configure;


import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component // 무언가 기능을 하는 단위다 라는 기능 묶음
public class CustomAccessDeniedHandler implements AccessDeniedHandler {
    // 오면 안될 때 들어온 경우에 대해서 이런 에러가 터지면 어떻게 할 건지
    // 로그인 상태가 아닐 때 끌어 내는 느낌


    // Http 데이터 왔다 갔다
    @Override // HttpServletRequest 고객 -> 서버 데이터 요청 서빙, HttpServletResponse 서버 -> 고객 주는 데이터 서빙
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.sendRedirect("/exception/access-denied");
        // 어디로 보낼지 : 서버 -> 고객 방식 response --> ExceptionController 로 보내줌
    }
}
