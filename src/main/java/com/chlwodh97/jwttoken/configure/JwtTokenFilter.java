package com.chlwodh97.jwttoken.configure;


import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import java.io.IOException;

@Component
@RequiredArgsConstructor            //Bean 시작 할 때 같이
public class JwtTokenFilter extends GenericFilterBean {
    private final JwtTokenProvider jwtTokenProvider;
    @Override // Chain 필터 끼리 엮는다. 필터 단계 설정
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String tokenFull = jwtTokenProvider.resolveToken((HttpServletRequest) request);
        String token = "";
        // 토큰이 없거나 다른 방식의 토큰 -> 다음 필터로 넘김
        if (tokenFull == null || tokenFull.startsWith("Bearer ")) {
            chain.doFilter(request,response);
            return; // 리턴이 되는 순간 끝남 -> 토큰 없다.
        }
        token = tokenFull.split("")[1].trim();
        if (!jwtTokenProvider.validateToken(token)) {
            chain.doFilter(request, response);
            return; // 리턴 되는 순간 끝남
        }
        Authentication authentication = jwtTokenProvider.getAuthentication(token);

                //Context 뜻 -> 환경, Holder -> 고정 -> 다같이 보려고 고정 한다.
                // 유실 될 수 있으니 일회용 출입증 고정 -> 고정 되어 있으니 보기만 하면 사용 가능
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request,response);
    }
}
