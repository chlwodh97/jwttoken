package com.chlwodh97.jwttoken.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MemberType {
    ROLE_GENERAL("일반회원"),
    ROLE_BOSS("사업자"),
    ROLE_MANAGEMENT("관리자");

    // String 이기 때문에 양식이 없다
    // ROLE_ 을 붙여서 권한인 것을 확인 할 수 있게 붙인다.
    private final String role;
}
