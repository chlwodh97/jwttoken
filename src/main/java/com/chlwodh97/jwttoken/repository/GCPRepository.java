package com.chlwodh97.jwttoken.repository;

import com.chlwodh97.jwttoken.entity.GCP;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GCPRepository extends JpaRepository<GCP, Long> {
}
