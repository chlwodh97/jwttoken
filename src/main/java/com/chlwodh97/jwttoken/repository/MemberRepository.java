package com.chlwodh97.jwttoken.repository;

import com.chlwodh97.jwttoken.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    Optional<Member> findByUsername(String username);
    // Optional 으로 해야 Throw 가 가능
    long countByUsername(String username);
}
