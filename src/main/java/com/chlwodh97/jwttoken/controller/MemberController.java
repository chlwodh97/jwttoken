package com.chlwodh97.jwttoken.controller;


import com.chlwodh97.jwttoken.enums.MemberType;
import com.chlwodh97.jwttoken.model.common.CommonResult;
import com.chlwodh97.jwttoken.model.member.MemberRequest;
import com.chlwodh97.jwttoken.service.MemberService;
import com.chlwodh97.jwttoken.service.ProfileService;
import com.chlwodh97.jwttoken.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;
    private final ProfileService profileService;


    @PostMapping("/join")
    @Operation(summary = "일반 회원 가입")
    public CommonResult setMember(@RequestBody @Valid MemberRequest request) {
        MemberType memberType = MemberType.ROLE_GENERAL;
        memberService.setMember(request, memberType);
        return ResponseService.getSuccessResult();
    }
    @PostMapping("/join-admin")
    @Operation(summary = "관리자 회원 만들기")
    public CommonResult setAdminMember(@RequestBody @Valid MemberRequest request) {
        MemberType memberType = MemberType.ROLE_MANAGEMENT;
        memberService.setMember(request, memberType);
        return ResponseService.getSuccessResult();
    }
}
