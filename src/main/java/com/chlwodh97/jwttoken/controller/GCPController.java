package com.chlwodh97.jwttoken.controller;


import com.chlwodh97.jwttoken.model.common.CommonResult;
import com.chlwodh97.jwttoken.service.GCPService;
import com.chlwodh97.jwttoken.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/gcp")
public class GCPController {
    private final GCPService gcpService;

    @PostMapping("/upload")
    public CommonResult saveBoardProfileImage(@RequestParam ("ImgFile")MultipartFile imgFile) throws IOException {
        gcpService.saveBoardProfileImage(imgFile);

        return ResponseService.getSuccessResult();
    }
}
