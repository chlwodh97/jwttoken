package com.chlwodh97.jwttoken.controller;



import com.chlwodh97.jwttoken.exception.CAccessDeniedException;
import com.chlwodh97.jwttoken.exception.CEntryPointException;
import com.chlwodh97.jwttoken.model.common.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exception")
public class ExceptionController {
    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() throws CAccessDeniedException {
        throw new ClassCastException(); // 예외 처리 해주기
        // : 회원만 가능한 ~ 입니다 의 경우
    }

    @GetMapping("entry-point")
    public CommonResult entryPointException() throws CEntryPointException {
        throw new ClassCastException(); // 예외 처리 해주기
        // : 로그인 중 권한이 있는 회원만 사용 가능한 ~~ 입니다 의 경우
    }
}
