package com.chlwodh97.jwttoken.controller;

import com.chlwodh97.jwttoken.enums.MemberType;
import com.chlwodh97.jwttoken.model.common.SingleResult;
import com.chlwodh97.jwttoken.model.member.LoginRequest;
import com.chlwodh97.jwttoken.model.member.LoginResponse;
import com.chlwodh97.jwttoken.service.LoginService;
import com.chlwodh97.jwttoken.service.ResponseService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login")
public class LoginController {
    private final LoginService loginService;
    @PostMapping("/web/admin")
    public SingleResult<LoginResponse> doLoginAdmin(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_MANAGEMENT,loginRequest,"WEB"));
    }

    @PostMapping("/app/user")
    public SingleResult<LoginResponse> doLoginUser(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_GENERAL,loginRequest,"APP"));
    }
    @PostMapping("/app/owner/user")
    public SingleResult<LoginResponse> doLoginOwnerUser(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_BOSS,loginRequest,"APP"));
    }
}
