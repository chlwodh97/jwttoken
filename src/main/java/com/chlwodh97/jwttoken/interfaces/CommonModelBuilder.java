package com.chlwodh97.jwttoken.interfaces;


/**
 * 빌드용
 */
public interface CommonModelBuilder<T> {
    T build();
}

