package com.chlwodh97.jwttoken;

import jakarta.annotation.PostConstruct;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.File;
import java.net.URI;

@SpringBootApplication
public class JwtTokenApplication {
	public static void main(String[] args) {
		SpringApplication.run(JwtTokenApplication.class, args);
	}
}
