package com.chlwodh97.jwttoken.model.member;


import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class LoginRequest {
    @NotNull
    @Length(min = 5, max = 20)
    private String username;

    @NotNull
    @Length(min = 5, max = 20)
    private String Password;
}
