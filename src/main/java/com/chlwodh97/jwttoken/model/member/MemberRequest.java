package com.chlwodh97.jwttoken.model.member;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

@Getter
@Setter
public class MemberRequest {
    @NotNull
    private String name;
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    private String passwordRe;
    @NotNull
    private Boolean isMan;
    @NotNull
    private LocalDate dateBirth;
    @NotNull
    private String phoneNumber;
    @NotNull
    private String address;
}
