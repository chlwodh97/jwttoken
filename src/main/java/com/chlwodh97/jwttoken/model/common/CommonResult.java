package com.chlwodh97.jwttoken.model.common;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonResult {
    private String msg;
    private Integer code;
    private Boolean isSuccess;
}
