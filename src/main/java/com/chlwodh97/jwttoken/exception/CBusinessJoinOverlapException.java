package com.chlwodh97.jwttoken.exception;

public class CBusinessJoinOverlapException extends RuntimeException{

    public CBusinessJoinOverlapException(String msg, Throwable t) {
        super(msg,t);
    }
    public CBusinessJoinOverlapException(String msg) {
        super(msg);
    }
    public CBusinessJoinOverlapException() {
        super();
    }
}