package com.chlwodh97.jwttoken.exception;

public class CMemberNotFoundUsername extends RuntimeException {
    public CMemberNotFoundUsername(String msg, Throwable t) {
        super(msg,t);
    }
    public CMemberNotFoundUsername(String msg) {
        super(msg);
    }
    public CMemberNotFoundUsername() {
        super();
    }
}
