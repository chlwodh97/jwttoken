package com.chlwodh97.jwttoken.exception;

public class CEntryPointException extends RuntimeException{
    public CEntryPointException(String msg, Throwable t) {
        super(msg,t);
    }
    public CEntryPointException(String msg) {
        super(msg);
    }
    public CEntryPointException() {
        super();
    }
}
