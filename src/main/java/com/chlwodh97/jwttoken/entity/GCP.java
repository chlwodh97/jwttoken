package com.chlwodh97.jwttoken.entity;


import com.chlwodh97.jwttoken.interfaces.CommonModelBuilder;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GCP {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String imgName;


    private GCP(Builder builder) {
        this.imgName = builder.imgName;
    }

    public static class Builder implements CommonModelBuilder<GCP>{
        private final String imgName;

        public Builder(String imgName) {
            this.imgName = imgName;
        }

        @Override
        public GCP build() {
            return new GCP(this);
        }
    }
}
