package com.chlwodh97.jwttoken.entity;

import com.chlwodh97.jwttoken.enums.MemberType;
import com.chlwodh97.jwttoken.interfaces.CommonModelBuilder;
import com.chlwodh97.jwttoken.model.member.MemberRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 40, unique = true)
    private String username;

    @Column(nullable = false,columnDefinition = "TEXT")
    private String password;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 15)
    private MemberType memberType;

    @Column(nullable = false)
    private Boolean isMan;

    @Column(nullable = false)
    private LocalDate dateBirth;

    @Column(nullable = false, length = 13)
    private String phoneNumber;

    @Column(length = 40)
    private String address;

    @Column(nullable = false)
    private LocalDateTime dateMember;

    private LocalDateTime dateChangeMember;

    private LocalDateTime dateOutMember;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(memberType.toString()));
        // 컬렉션 : 수집하는 방법
        // 싱글톤 : 권한은 하나다. 확실히 얘기를 해준다
        // SimpleGrantedAuthority 의 필요 타입은 String 때문에 toString 해줌
    }

    @Override   // 만료 안됐니? -> 언제 까지 쓸 수 있나 유효기간 정하기
                 // 넷플같은 유료 회원 돈 안냈을 때 정도
    public boolean isAccountNonExpired() {
        return true;
    }

    //정지 / 인증만료 / 활성화 / 같은 경우는 날짜형식 으로 true 값을 줄 수있음
    //ex) stopDay → 2024-04-01 ) -> return LocalDate.now().isAfter(stopDay)

    @Override   // 정지 안됐니? -> 정지여부 판단
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override   // 너에 대한 인증 만료 됐니? -> 개인 신분 확인
                 // 개인에 대한 신변 확인
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override   // 활성화 됐니 -> 휴면 회원 활성화가 되지 않았음
    public boolean isEnabled() {
        return true;
    }

    /**
     * 대량등록
     */
    private Member(BuilderCsv builder) {
        this.name = builder.name;
        this.username = builder.username;
        this.password = builder.password;
        this.isMan = builder.isMan;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
        this.dateMember = builder.dateMember;
    }
    private Member(Builder builder) {
        this.name = builder.name;
        this.username = builder.username;
        this.password = builder.password;
        this.memberType = builder.memberType;
        this.isMan = builder.isMan;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
        this.address = builder.address;
        this.dateMember = builder.dateMember;
        this.dateChangeMember = builder.dateChangeMember;
        this.dateOutMember = builder.dateOutMember;
    }


    /**
     * 대량등록
     */
    public static class BuilderCsv implements CommonModelBuilder<Member> {
        private final String name;
        private final String username;
        private final String password;
        private final Boolean isMan;
        private final LocalDate dateBirth;
        private final String phoneNumber;
        private final LocalDateTime dateMember;


        /**
         * 대량등록
         */
        public BuilderCsv(String[] cols) {
            this.name = (cols[2]);
            this.username = (cols[3]);
            this.password = (cols[4]);
            this.isMan = (Boolean.valueOf(cols[5]));
            this.dateBirth = (LocalDate.parse(cols[6]));
            this.phoneNumber = (cols[7]);
            this.dateMember = (LocalDateTime.parse(cols[8]));
        }
        /**
         * 대량등록
         */
        @Override
        public Member build() {
            return new Member(this);
        }
    }
    public static class Builder implements CommonModelBuilder<Member> {

        private final String name;
        private final String username;
        private final String password;
        private final MemberType memberType;
        private final Boolean isMan;
        private final LocalDate dateBirth;
        private final String phoneNumber;
        private final String address;
        private final LocalDateTime dateMember;
        private final LocalDateTime dateChangeMember;
        private final LocalDateTime dateOutMember;

        public Builder(MemberRequest memberRequest, MemberType memberType) {
            this.name = memberRequest.getName();
            this.username = memberRequest.getUsername();
            this.password = memberRequest.getPassword();
            this.memberType = memberType;
            this.isMan = memberRequest.getIsMan();
            this.dateBirth = memberRequest.getDateBirth();
            this.phoneNumber = memberRequest.getPhoneNumber();
            this.address = memberRequest.getAddress();
            this.dateMember = LocalDateTime.now();
            this.dateChangeMember = null;
            this.dateOutMember = null;
        }
        @Override
        public Member build() {
            return new Member(this);
        }
    }


}

