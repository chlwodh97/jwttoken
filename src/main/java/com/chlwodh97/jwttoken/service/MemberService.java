package com.chlwodh97.jwttoken.service;


import com.chlwodh97.jwttoken.entity.Member;
import com.chlwodh97.jwttoken.enums.MemberType;
import com.chlwodh97.jwttoken.exception.CMemberNotFoundUsername;
import com.chlwodh97.jwttoken.exception.CMemberPasswordException;
import com.chlwodh97.jwttoken.lib.CommonCheck;
import com.chlwodh97.jwttoken.model.member.MemberRequest;
import com.chlwodh97.jwttoken.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;

    // 관리자는 관리자만 등록 가능해야하고 일반회원은 아무나 등록가능해야한다.
    // 아무나 관리자로 등록하면 안된다
    // 그러니 컨트롤러에서 일반회원가입 , 관리자 등록 이렇게 두개로 나눠서 구현해야한다.

    //어차피 비슷한 코드인데 굳이 나눌 필요 없다.
    //그래서 어떤 회원그룹에 가입시킬지 서비스에서 받고..
    public void setMember(MemberRequest request,MemberType memberType) {

        if (!CommonCheck.checkUsername(request.getUsername())) throw new CMemberNotFoundUsername(); // 유효한 아이디가 아닙니다.
        if (!request.getPassword().equals(request.getPasswordRe())) throw new CMemberPasswordException(); // 비밀번호가 일치하지 않습니다.
        if (!isNewUsername(request.getUsername())) throw new CMemberPasswordException(); // 중복된 아이디가 존재합니다.

        request.setPassword(passwordEncoder.encode(request.getPassword()));
        Member member = new Member.Builder(request,memberType).build();
        memberRepository.save(member);
    }

    //왜 private 이냐.. set 멤버에서만 쓸 수 있어야 하기 때문에 ..?
    // 그래서 왜 다른 곳에서 이미 있는 아이디를 찾지 못하게 하려고?
    private Boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);
        return dupCount <= 0;
    }
}
