package com.chlwodh97.jwttoken.service;

import com.chlwodh97.jwttoken.entity.Member;
import com.chlwodh97.jwttoken.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProfileService {
    private final MemberRepository memberRepository;


    public Member getDate() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Member member = memberRepository.findByUsername(username).orElseThrow(); // 회원정보가 없습니다.

        return member;
    }
}
