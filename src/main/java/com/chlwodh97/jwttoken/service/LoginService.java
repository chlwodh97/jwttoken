package com.chlwodh97.jwttoken.service;


import com.chlwodh97.jwttoken.configure.JwtTokenProvider;
import com.chlwodh97.jwttoken.entity.Member;
import com.chlwodh97.jwttoken.enums.MemberType;
import com.chlwodh97.jwttoken.exception.CMemberNotFoundUsername;
import com.chlwodh97.jwttoken.exception.CMemberPasswordException;
import com.chlwodh97.jwttoken.model.member.LoginRequest;
import com.chlwodh97.jwttoken.model.member.LoginResponse;
import com.chlwodh97.jwttoken.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;

    // 로그인 타입은 WEB or APP (WEB = 10시간 , APP = 1년)
    public LoginResponse doLogin(MemberType memberType, LoginRequest loginRequest, String longinType) {
        Member member = memberRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CMemberNotFoundUsername::new);
        //등록된 아이디 없음

        if (!member.getMemberType().equals(memberType)) throw new CMemberNotFoundUsername();
        // 일반회원이 관리자 로그인 하려거나 이런 경우 경우 -> 메세지는 회원정보가 없습니다. 로 던짐
        if (!passwordEncoder.matches(loginRequest.getPassword(),member.getPassword())) throw new CMemberPasswordException();

        String token = jwtTokenProvider.createToken(String.valueOf(member.getUsername()),member.getMemberType().toString(), longinType);

        return new LoginResponse.Builder(token, member.getName()).build();
    }
}
