package com.chlwodh97.jwttoken.service;


import com.chlwodh97.jwttoken.entity.Member;
import com.chlwodh97.jwttoken.lib.CommonFile;
import com.chlwodh97.jwttoken.lib.CustomMultipartFile;
import com.chlwodh97.jwttoken.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

@Service
@RequiredArgsConstructor
public class TempService {
    private final MemberRepository memberRepository;
    private final GCPService gcpService;
    private int FIX_SIZE = 1000;

    private MultipartFile convertBufferedImageToMultipartFile(BufferedImage image) throws IOException{
        ByteArrayOutputStream out = new ByteArrayOutputStream();
            ImageIO.write(image, "image/jpg", out);
        byte[] bytes = out.toByteArray();
        return new CustomMultipartFile(bytes, "image", "image/jpg", "jpg", bytes.length);
    }
    /**
     * 이미지 파일 비율 맞추고 업로드
     */
    public void setImgFile(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.mltipartToFile(multipartFile);
        BufferedImage bufferedImage = ImageIO.read(file);


        int oldWidth = bufferedImage.getWidth();
        int oldHeight = bufferedImage.getHeight();
        int newWidth;
        int newHeight;

        if (oldWidth > oldHeight) { // max는 1000이 넘지 않게.. 사진의 width가 사진의 height 보다 크면 -> width는 1000이고. height 는 1000 곱하기 원래 height 나누기
            newWidth = FIX_SIZE;
            newHeight = FIX_SIZE * oldHeight / oldWidth;
        } else {
            newWidth = FIX_SIZE * oldWidth / oldHeight;
            newHeight = FIX_SIZE;
        }
        // 새로운 이미지 파일의 크기 설정
        Image resizeImg = bufferedImage.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
        // 뉴 이미지는 새로운 파일 -> 새로운 파일에 write 해서 쓰는거다. 다시 그 파일을 읽어오면 형태가 file
        // -> file을 mult파일로 바꾸면 간단하다.
        BufferedImage newImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_BGR);
        Graphics graphics = newImage.getGraphics();
        graphics.drawImage(resizeImg, 0, 0, null);
        graphics.dispose();

        //파일을 멀티파트로 바꾸는 건 쉽다 .. - > 그래픽을 멀티파트로 바꾸라는
        File newFile = new File("newnow.jpg");
        ImageIO.write(newImage,"jpg", newFile);

        BufferedImage  bufferedImage1 = ImageIO.read(newFile);
        System.out.println(bufferedImage1);

        //File file = new File(multipartFile.getOriginalFilename());multipartFile.transferTo(file);

        MultipartFile newCustomMultipartFile = convertBufferedImageToMultipartFile(bufferedImage1);

        gcpService.saveBoardProfileImage(newCustomMultipartFile);
    }


    /**
     * @param multipartFile 데이터 베이스에 회원 정보
     * @throws IOException 한번에 업로드
     */
    public void setMemberByFile(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.mltipartToFile(multipartFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        //버퍼에서 넘어온 String 한 줄을 임시로 담아 둘 변수 가 필요함
        String line = "";

        // 버퍼에선 번호를 줄 필요가 없다
        // 줄 번호 수동 체크를 위해 int 로 줄 번호 하나씩 1씩 증가 해서 기록 할 변수 필요함
        int index = 0;

        while ((line = bufferedReader.readLine()) != null) {
            if (index > 0) {
                String[] cols = line.split(",");
                if (cols.length == 9) {
                    memberRepository.save(new Member.BuilderCsv(cols).build());
                }
            }
            index++;
            // 자기 자신에 ++
            // index는 몇번 째 줄인지 표시 + 0번째 데이터 안보이기 -> 맨위의 설명란은 필요없으니
        }
        bufferedReader.close();
    }
}
