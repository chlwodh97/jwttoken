package com.chlwodh97.jwttoken.service;

import com.chlwodh97.jwttoken.exception.CAccessDeniedException;
import com.chlwodh97.jwttoken.exception.CMemberNotFoundUsername;
import com.chlwodh97.jwttoken.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    private final MemberRepository memberRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return memberRepository.findByUsername(username).orElseThrow(CMemberNotFoundUsername::new);


        //orElseThrow() 안에 예외 처리 메세지 넣기 가능

        // Member 가 UserDetails 를 implements (구현) 하고 있기 때문에
        // 리턴 타입이 UserDetails 모양과 달라도 괜찮음
    }
}
