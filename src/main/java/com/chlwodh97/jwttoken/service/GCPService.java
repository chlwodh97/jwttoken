package com.chlwodh97.jwttoken.service;


import com.chlwodh97.jwttoken.entity.GCP;
import com.chlwodh97.jwttoken.repository.GCPRepository;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class GCPService {
    @Value("${spring.cloud.gcp.storage.bucket}")
    private String bucketName;

    @Value("${spring.cloud.key}")
    private String GCP_KEY;

    private String getNewFileName(String originalName, String uuid) {
        return uuid + "_" + originalName;
    }

    public GCP saveBoardProfileImage(MultipartFile imageFile) throws IOException {
        // 경로가 틀려서 안들어 갔음!
        InputStream keyFile = ResourceUtils.getURL("classpath:" + GCP_KEY).openStream();
        Storage storage;
        //괄호 하나 빠져서 에러
        storage = StorageOptions.newBuilder()
                .setCredentials(GoogleCredentials.fromStream(keyFile))
                .build()
                .getService();

        String originalName = imageFile.getOriginalFilename();
        // 확장자
        String ext = imageFile.getContentType();
        // cdn 저장되는 이름
        String uuid = UUID.randomUUID().toString();

        // 파일은 https://storage.googleapis.com/{버킷_이름}/{UUID}를 통해 조회할 수 있음
        BlobInfo imageInfo = BlobInfo.newBuilder(bucketName, uuid)
                .setContentType(ext)
                .build();
        // 초기화
        BlobInfo blobInfo = null;

        try {
            //다시 시도
            blobInfo = storage.create(imageInfo, imageFile.getInputStream());

            return new GCP.Builder(getNewFileName(originalName, uuid)).build();

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
