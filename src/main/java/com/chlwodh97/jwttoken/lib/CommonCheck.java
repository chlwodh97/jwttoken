package com.chlwodh97.jwttoken.lib;

public class CommonCheck {
    public static boolean checkUsername(String username) {
        // 사용자 입력에 대한 검증 첫자a-zA-z 이런 뜻
        String pattern = "^[a-zA-z]{1}[a-zA-z0-9]{4,19}$";
        return username.matches(pattern);
    }
}
